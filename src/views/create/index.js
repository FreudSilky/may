import React, { Component } from 'react';
import firebase from '../../utils/Firebase';
class Create extends Component {

  constructor() {
    super();
    this.ref = firebase.database().ref();
    this.state = {
      formData: {
        nombre: '',
        aPaterno: '',
        aMaterno: '',
        dateNacimiento: "",
      },
    }
  }
  handleChange = (event) => {
    const { formData } = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({ formData });
  }
  componentDidMount() {

  }
  handleSubmit = (e) => {
    e.preventDefault();

    let data = {};
    const key = this.ref.child('empleado').push().key;
    data[`empleado/${key}`] = this.state.formData;
    this.ref.update(data).then(() => {
      this.setState({
        formData: {
          nombre: '',
          aPaterno: '',
          aMaterno: '',
          dateNacimiento: '',
        },
      });
      this.props.history.push("/")
    }).catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    return (
        <div className="container">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">
                Agregar
              </h3>
            </div>
            <div className="panel-body">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label for="title">Nombre:</label>
                  <input type="text" maxLength="18" className="form-control" name="nombre"  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
                </div>
                <div className="form-group">
                  <label for="title">Apellido Paterno:</label>
                  <input type="text" className="form-control" maxLength="18" name="aPaterno"  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
                </div>
                <div className="form-group">
                  <label for="title">Apellido Materno:</label>
                  <input type="text" className="form-control" maxLength="18" name="aMaterno"  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
                </div>
                <div className="form-group">
                  <label for="title">Fecha:</label>
                  <input type="date" className="form-control" name="dateNacimiento"  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
                </div>
                <div style={{textAlign:"center"}}>
                <button type="submit" className="btn btn-success">Agregar</button>
                <a href="/" className="btn btn-danger">
                            Cancelar
                          </a>
                </div>
              </form>
            </div>
          </div>
        </div>)

    
   }
   }
         
      
          
          export default Create;
