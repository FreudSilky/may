import React, { Component } from 'react';
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import axios from 'axios';
// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'colum',
    backgroundColor: 'white'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});

class MyDocument extends Component {
 datos(){
   try {
     
  const {empleado} =this.props;

  return(
    empleado.map(element => 
      <View style={styles.section} key={element.id}>
      <Text>Nombre: {element.nombre} {element.aPaterno} {element.aMaterno}</Text>
      <Text>Fecha de nacimiento: {element.dateNacimiento}</Text>
      </View>
    )
  )
  
} catch (error) {
  const name = "Crud";
  const email = "maylleydemay@gmail.com";
  const message = error.message;
  axios({
    method: "POST",
    url: "https://app-freud.herokuapp.com/send",
    data: {
      name: name,
      email: email,
      messsage: message
    }
  }) 
 }
    
 }
componentDidMount(){

}
  
  render() {
      return(
        <Document>
        <Page size="A4" style={styles.page}>
            {this.datos()}
        </Page>
      </Document>
      )}

}


export default MyDocument;
