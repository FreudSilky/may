import React, { Component } from 'react';
import firebase from '../../utils/Firebase';
import axios from 'axios';

class Edit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      formData: {},
    };
  }
  consulta() {
    try {
      const { id } = this.props.match.params;
      firebase.database().ref('empleado/' + id).once('value', snapshot => {
          let formData = {

            id: snapshot.key,
            nombre: snapshot.val().nombre,
            aPaterno: snapshot.val().aPaterno,
            aMaterno: snapshot.val().aMaterno,
            dateNacimiento: snapshot.val().dateNacimiento,

          }
          this.setState({
            formData,
          });

      })

    } catch (error) {
      const name = "Crud";
      const email = "maylleydemay@gmail.com";
      const message = error.message;
      axios({
        method: "POST",
        url: "https://app-freud.herokuapp.com/send",
        data: {
          name: name,
          email: email,
          messsage: message
        }
      })
    }

  }

  componentDidMount() {
    this.consulta();
  }

  handleChange = (event) => {
    const { formData } = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({ formData });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const validate = this.state.formData;
    let data = Object.assign({}, validate);
    const ref = firebase.database().ref().child(`empleado/${this.state.formData.id}`);
    ref.update(data).then(() => {
      this.setState({
        data: {}
      });
      this.props.history.push("/");
    })
  }
  render() {
      
      const { formData } = this.state;
        return (<div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              Editar
            </h3>
          </div>
          <div className="panel-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label for="title">Nombre:</label>
                <input type="text" className="form-control" maxLength="18" name="nombre" value={formData.nombre} onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
              </div>
              <div className="form-group">
                <label for="title">Apellido Paterno:</label>
                <input type="text" className="form-control" maxLength="18" name="aPaterno" value={formData.aPaterno} onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
              </div>
              <div className="form-group">
                <label for="title">Apellido Materno:</label>
                <input type="text" className="form-control"  maxLength="18" name="aMaterno" value={formData.aMaterno}  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
              </div>
              <div className="form-group">
                <label for="title">Fecha:</label>
                <input type="date" className="form-control" name="dateNacimiento" value={formData.dateNacimiento}  onChange={this.handleChange} pattern="[A-Za-z]+" title="error" placeholder="Nombre" required  />
              </div>
              <div style={{textAlign:"center"}}>
              <button type="submit" className="btn btn-success">Agregar</button>
              <a href="/" className="btn btn-danger">
                          Cancelar
                        </a>
              </div>
            </form>
          </div>
        </div>
      </div>
        );
      }
    
    }

export default Edit;
