import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

const config = {
  apiKey: "AIzaSyAtT6bTaJjAl0aXKngmoYFz3--NeIHHOKI",
    authDomain: "fir-a29a0.firebaseapp.com",
    databaseURL: "https://fir-a29a0.firebaseio.com",
    projectId: "fir-a29a0",
    storageBucket: "fir-a29a0.appspot.com",
    messagingSenderId: "157668519867",
    appId: "1:157668519867:web:965c0c75dae48182eede9e",
    measurementId: "G-MM2CLPEE7C"
};
 

export default firebase.initializeApp(config);
