import React, { Component } from 'react';
import firebase from '../../utils/Firebase';



export default class MaterialTableDemo extends Component {
    _isMoment = false;

    constructor(props) {
        super(props);
        this.state = {
            empleado: [],
            open: false,
            id: "",
        };
    }

    handleClickOpen = (id) => {
        this.setState({
            open: true,
            id,
        });
    };

    handleClose = () => {
        this.setState({
            open: false,
            id: "",
        });
    };
    onCollectionUpdate = () => {

        firebase.database().ref('empleado/').once('value', snapshot => {
            let empleado = [];
            snapshot.forEach(row => {
                empleado.push({
                    id: row.key,
                    nombre: row.val().nombre,
                    aPaterno: row.val().aPaterno,
                    aMaterno: row.val().aMaterno,
                    dateNacimiento: row.val().dateNacimiento,
                })
            });
            if (this._isMoment) {
                this.setState({
                    empleado,
                });
            }

        })
    }
    delete = id => {
        if(window.confirm('Esta seguro de eliminar')){
        console.log(id);
        const ref = firebase.database().ref().child(`empleado/${id}`);
        ref.remove().then(() => {
            this.setState({
                open: false,
                id: "",
            });
            this.onCollectionUpdate();
        }).catch(function (error) {
        });
    }
    }
    
    componentDidMount() {
        this.onCollectionUpdate();
    }
    componentWillMount() {
        this._isMoment = true;
    }

    render() {
        return (
            <div>
                <div className="row">
                {this.state.empleado.map(board =>
                        <div className="col-sm-3" key={board.id}>
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">Empleado</h5>
                                    <p className="card-text">Nombre:{board.nombre} {board.aPaterno} {board.aMaterno}</p>
                                    <p className="card-text">Fecha de nacimiento:{board.dateNacimiento}</p>
                                    <a href={`/editar/${board.id}`} className="btn btn-danger"> Editar</a>
                                    <a href={`/pdf/${board.id}`}  target="blank" className="btn btn-danger" > PDF</a>
                                    <button onClick={() => this.delete(board.id)} className="btn btn-danger">Eliminar</button>
                                </div>
                            </div>
                        </div>
                    
                )}
                </div>
            </div>
        )
    }
}

